.PHONY: all clean

all: $(patsubst %.slvs,%.svg,$(wildcard *.slvs))
	@find -mindepth 2 -name Makefile -exec sh -c 'make -C "$$(dirname "{}")"' \;

%.svg: %.slvs
	solvespace-cli export-view --bg-color on --chord-tol 1 --view isometric --output $@ $<

clean:
	@echo Use git clean...
